import json
import io
import urllib.parse
import boto3
import requests
import pandas as pd
from app_logging import get_logger
import config

log = get_logger(__name__)
s3 = boto3.client('s3')


def trigger_api(payload):
    """Trigger http reset API using post method.
    Args:
        payload: dict or json format
    """
    headers = {"Content-Type": "application/json"}
    try:
        response = requests.post(config.API_URL, data=json.dumps(payload), headers=headers)
        log.info("status code: %s", response.status_code)
        if response.headers.get("Content-Type") == "application/json":
            response = response.json()
            print("Response data is ", response)
        else:
            response = response.text
            print("Response data is ", response)
    except ValueError:
        log.exception("Response body is empty")
    except requests.exceptions.Timeout:
        log.exception("API request failed due to request timeout.")
    except requests.exceptions.RequestException:
        log.exception("API request failed due to a request exception.")


def lambda_handler(event, context):
    """Read file from s3 using eventbridge and call mock api"""
    bucket = event['Records'][0]['s3']['bucket']['name']
    file = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    log.info("Uploaded file name is %s", file)
    try:
        response = s3.get_object(Bucket=bucket, Key=file)
        try:
            data = response['Body'].read()
            df = pd.read_excel(io.BytesIO(data))
            print("File data is \n", df)
        except Exception:
            log.exception("Error occured on reading excel file")

        # Trigger API end point
        trigger_api({"file_name": file})

    except Exception:
        log.exception('Error getting object %s from bucket %s. Make sure they exist and your bucket is in the same'
                      ' region as this function.',file, bucket)

