Create a lambda function
![ScreenShot](images/lambda.png)

create event bridge and set rules
![ScreenShot](images/event-bridge-rule.png)

create api end point in apigateway as mock
![ScreenShot](images/api-gateway.png)

Create s3 bucket
![ScreenShot](images/s3.png)

Verify cloud watch
![ScreenShot](images/CloudWatch-log.png)

create layer using requirement.txt  

These manual task can be automated using serverless framework.