import logging


def get_logger(module_name):
    """Get the logger object for the given module name"""
    # Create a custom logger
    logger_object = logging.getLogger(module_name)
    logger_object.propagate = False
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.DEBUG)

    stream_handler_formatter = logging.Formatter('%(asctime)s - [%(levelname)s] - %(module)s : %(message)s')
    stream_handler.setFormatter(stream_handler_formatter)

    logger_object.addHandler(stream_handler)
    logger_object.setLevel(logging.DEBUG)
    return logger_object
